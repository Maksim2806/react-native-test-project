import React from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import WeatherCards from './WeatherCards';



export default function ForecastPage() {
  return (
    <View style={styles.forecastPage}>
      <View style={styles.forecastPage__title}>
          <Text style={styles.forecastPage__first}>Weather</Text>
          <Text style={styles.forecastPage__second}>forecast</Text>
      </View>
      <ScrollView style={{flex: 0.5, height: 1500}}>
        <WeatherCards />
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  forecastPage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
  },
  forecastPage__title: {
    flex: 1,
    top: 40,
    width: 280,
    height: 120,
    marginBottom: 50    
  },
  forecastPage__first: {
    fontSize: 40,
    textAlign: 'left',
    color: '#FFFFFF'
  },
  forecastPage__second: {
    fontSize: 40,
    textAlign: 'right',
    color: '#FFFFFF'
  }
});