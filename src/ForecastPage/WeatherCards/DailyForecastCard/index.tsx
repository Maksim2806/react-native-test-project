import React, { useCallback, useMemo, useState } from 'react'
import  WeatherCard  from '../../../components/WeatherCard'
import { CitiesOptions } from '../../../constants/cities'
import { DailyForecastResponse } from '../../../service/types/fetchWeatherForecastTypes'
import { IServerError } from '../../../service/types'
import { fetchDailyForecast } from '../../../service/fetchWeatherForecast'
import Carousel from '../../../components/Carousel'
import WeatherCardItem from '../../../components/WeatherCardItem'
import { formatUnixTimeToDate } from '../../../utils'
import ErrorContainer from '../../../components/ErrorContainer'
import Select, {  IOption } from '../../../components/Select'
import RNPickerSelect from '@react-native-picker/picker'

interface ICityOption extends IOption {
  // key: string | number
  // name: string
  value: {
    lon: number,
    lat: number,
  };
}

const DailyForecastCard = () => {
  const [data, setData] = useState<DailyForecastResponse | undefined>()
  const [error, setError] = useState<IServerError | undefined>()
  const [selectCityValue, setSelectCityValue] =
    useState<ICityOption | undefined>()

  const onSelectCity = useCallback(async (selectedOption: ICityOption) => {
    setSelectCityValue(selectedOption)
    setError(undefined)
    try {
      const data = await fetchDailyForecast({
        lat: selectedOption.value.lat,
        lon: selectedOption.value.lon
      })
      setData(data)
    } catch (e) {
      setError(e)
    } finally {
    }
  }, [])

  const content = useMemo(() => {
    if (error) {
      return <ErrorContainer message={error.message} />
    }
    if (!data) return
    const carouselContent = data.daily
      .slice(0, -1)
      .map(({ dt, weather, temp }) => {
        return (
          <WeatherCardItem
            item={{
              date: formatUnixTimeToDate(dt),
              iconName: weather[0].icon,
              temperature: temp.day
            }}
          />
        )
      })
    // return <Carousel content={carouselContent} />
  }, [data, error])

  return (
    <WeatherCard
      title="7 Days Forecast"
      inputComponents={[
        
        // <Select
        //   value={selectCityValue}
        //   options={CitiesOptions}
        //   onChange={onSelectCity}
        // />
      ]}
      content={content}
    />
  )
}

export default DailyForecastCard 
