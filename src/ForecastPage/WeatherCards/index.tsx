import React from 'react'
import { ScrollView, StyleSheet, View } from 'react-native'
import DailyForecastCard from './DailyForecastCard'
import HistoricalForecastCard from './HistoricalForecastCard'

const WeatherCards = () => {
  return (
    <View style={styles.container}>
      <DailyForecastCard />
      <HistoricalForecastCard />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center'
  }
});

export default WeatherCards