import React, { useCallback } from 'react'
import { StyleSheet, TextInput } from 'react-native'
import { formatDateToString } from '../../utils'

interface Props {
    onChange: (date: Date) => void
    value?: Date;
    defaultValue?: Date
    placeholder?: string
  }

const DateInput:React.FC<Props> = ({ value, defaultValue, onChange, placeholder = 'Select Date' }) => {
  const handleChange = useCallback((e: any) => {
    onChange(new Date(e.target.value))
  }, [onChange])

  return (
        <TextInput
          value={value && formatDateToString(value)}
          style={styles.input}
          onChange={handleChange}
          defaultValue={defaultValue && formatDateToString(defaultValue)}
          placeholder={placeholder}
    />
  )
}

const styles = StyleSheet.create({
  input: {
    position: 'relative',
    width: 252,
    height: 48,
    padding: 16,
    // backgroundColor: rgba(128, 131, 164, 0.06),
    // border: 2px solid rgba(128, 131, 164, 0.2),
    // border-radius: 8,
    // font-size: 16,
    // line-height: 42,
    // text-align: left
  }
})
export default DateInput
