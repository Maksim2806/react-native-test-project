import React from 'react'
import { StyleSheet, Image, Text, View } from 'react-native'

const WeatherCardPlaceholder = () => {
  return (
    <View style={styles.weatherCardPlaceholder}>
      <Image
        style={styles.weatherCardPlaceholder__img}
        source={require('./assets/Academy-Weather-bg160.png')}
        accessibilityLabel="placeholder"
      />
      <Text style={styles.weatherCardPlaceholder__figcaption}>
        Fill in all the fields and the weather will be displayed
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  weatherCardPlaceholder: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  weatherCardPlaceholder__img: {
    flex: 1,
    width: '50%',
    height: '50%',
    resizeMode: 'contain'
  },
  weatherCardPlaceholder__figcaption: {
    flex: 1,
    fontSize: 16,
    fontWeight: '700',
    color: '#8083A4',
    textAlign: 'center'
  }
})
export default WeatherCardPlaceholder
