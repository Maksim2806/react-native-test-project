import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import WeatherCardPlaceholder from '../WeatherCardPlaceholder'

interface Props {
  title: string;
  content?: React.ReactNode;
  inputComponents: React.ReactNode[];
}

const WeatherCard: React.FC<Props> = ({ title, content, inputComponents }) => {
  return (
    <View style={styles.weatherCard}>
      <Text style={styles.weatherCard__title}>{title}</Text>
      <View style={styles.weatherCard__inputs}>
        {inputComponents.map((inputComponent) => inputComponent)}
      </View>
      <View style={styles.weaterCard__content}>
        {content || <WeatherCardPlaceholder />}
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  weatherCard: {
    flex: 1,
    alignItems: 'center',
    height: 464,
    width: 300,
    padding: 10,
    backgroundColor: '#FFF',
    marginBottom: 10
  },
  weatherCard__title: {
    flex: 1,
    paddingHorizontal: 24,
    paddingVertical: 16,
    fontSize: 32,
    fontWeight: '700',
    color: '#2C2D76',
  },
  weatherCard__inputs: {
    flex: 1
  },
  weaterCard__content: {
    flex: 3
  }
})

export default WeatherCard
