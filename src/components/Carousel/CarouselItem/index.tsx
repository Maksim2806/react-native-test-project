import React from 'react'
import classnames from 'classnames'
import { StyleSheet, View } from 'react-native'

interface Props {
  item: React.ReactNode;
  isLastItem?: boolean;
}

const CarouselItem: React.FC<Props> = ({ item, isLastItem }) => {
  return (
    <View
      // style={classnames({
      //   styles.carouselItem: true,
      //   carouselItem_noMargin: !!isLastItem
      // })}
    >
      {item}
    </View>
  )
}

const styles = StyleSheet.create({
  carouselItem: {
    width: 174,
    height: 238,
    marginRight: 10
  },
  carouselItem_noMargin: {
    marginRight: 0
  }
})

export default CarouselItem
