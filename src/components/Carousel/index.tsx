import React, { useRef, useCallback } from 'react'
import CarouselItem from './CarouselItem'
import { StyleSheet, View, Image, Button, ScrollView } from 'react-native'

interface Props {
  content: React.ReactNode[];
}

const Carousel: React.FC<Props> = ({ content }) => {
  const carouselRef = useRef<HTMLDivElement | null>(null)

  const handleLeftClick = useCallback(() => {
    const scrollLeft = carouselRef.current?.scrollLeft
    carouselRef.current?.scrollTo({
      left: scrollLeft !== undefined ? scrollLeft - 184 : 0,
      behavior: 'smooth'
    })
  }, [carouselRef])

  const handleRightClick = useCallback(() => {
    const scrollLeft = carouselRef.current?.scrollLeft
    carouselRef.current?.scrollTo({
      left: scrollLeft !== undefined ? scrollLeft + 184 : 0,
      behavior: 'smooth'
    })
  }, [carouselRef])

  return (
    <View style={styles.carousel__wrapper}>
      <ScrollView style={styles.carousel} ref={carouselRef}>
        <Button
          onPress={handleLeftClick}
          style="carousel__arrow carousel__leftArrow"
        >
          <Image
            style="carousel__leftArrowImage"
            src={require('./assets/chevron-left.svg')}
            alt="arrow-left"
          />
        </Button>
        <View style="carousel__container">
          {content.map((item, idx) => {
            return (
              <CarouselItem
                key={idx}
                item={item}
                isLastItem={idx === content.length - 1}
              />
            )
          })}
        </View>
        <Button
          onPress={handleRightClick}
          style="carousel__arrow carousel__rightArrow"
        >
          <Image
            style="carousel__rightArrowImage"
            src={require('./assets/chevron-left-1.svg')}
            alt="right-arrow"
          />
        </Button>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  carousel__wrapper: {
    position: 'relative',
    width: '100%',
    height: 250
  },
  carousel: {
    overflow: 'hidden',
    width: '100%',
    height: '100%',
  },
  carousel__arrow: {
    position: 'absolute',
    top: '45%'
  },
  carousel__leftArrow: {
    left: '-5%'
  },
  carousel__leftArrowImage: {

  },
  carousel__container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  carousel__rightArrow: {
    right: '-5%'
  }
})

export default Carousel
