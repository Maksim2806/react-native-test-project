import React from 'react'
import {StyleSheet, Text, View} from 'react-native'

interface Props {
  message?: string;
}

const ErrorContainer: React.FC<Props> = ({
  message = 'Oooops, something went wrong ...'
}) => {
  return (
    <View style={styles.errorContainer}>
      <Text style={styles.errorContainer__message}>{message}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  errorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%'
  },
  errorContainer__message: {
    color: 'crimson'
  }
})

export default ErrorContainer
