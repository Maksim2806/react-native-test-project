import React, { useCallback, useEffect, useMemo, useState } from 'react'
import cn from 'styles'
import RNPickerSelect from '@react-native-picker/picker'

export interface IOption {
  key: string | number
  name: string
  value: any
}

export interface SelectProps {
  options: Array<IOption>
  onChange: (option: IOption) => void
  value?: IOption
  defaultValue?: IOption
  placeholder?: string
}

export const Dropdown = () => {
  return (
      <RNPickerSelect
          onValueChange={(value) => console.log(value)}
          items={[
              { label: 'Football', value: 'football' },
              { label: 'Baseball', value: 'baseball' },
              { label: 'Hockey', value: 'hockey' },
          ]}
      />
  );
};

// const Select: React.FC<SelectProps> = ({ options, value, onChange, defaultValue, placeholder = 'Select city' }) => {
//   const [isOptionsMenuOpen, setIsOptionsMenuOpen] = useState<boolean>(false)

//   const selectValue = useMemo(() => value || defaultValue, [value, defaultValue])

//   const isSelectElem = (style: string) => style.search(/\bselect\b/) !== -1
//   const isSelectOptionsElem = (style: string) => style.search(/\bselect-options\b/) !== -1
//   const isSelectArrowElem = (style: string) => style.search(/\bselect__img-arrow\b/) !== -1

//   const toggleOptionsMenu = useCallback((e: any) => {
//     const elemstyle = e.target.style || ''
//     if (isSelectElem || isSelectArrowElem(elemstyle)) {
//       setIsOptionsMenuOpen((isOpen) => !isOpen)
//     }
//   }, [])

//   const handleSelect = useCallback((option: IOption) => (e: React.SyntheticEvent<HTMLDivElement>) => {
//     e.stopPropagation()
//     setIsOptionsMenuOpen(false)
//     onChange(option)
//   }, [onChange])

//   const onClickOutside = useCallback((e:any) => {
//     const composedPath = e.composedPath()

//     const needToCloseMenu = !composedPath.some((el: any) => {
//       const elemstyle = el.style || ''
//       return isSelectElem(elemstyle) || isSelectOptionsElem(elemstyle)
//     })

//     if (needToCloseMenu) {
//       setIsOptionsMenuOpen(false)
//     }
//   }, [])

//   useEffect(() => {
//     document.addEventListener('click', onClickOutside)
//     return () => {
//       document.removeEventListener('click', onClickOutside)
//     }
//   }, [onClickOutside])

//   // return (<RNPickerSelect style={cn({
//   //   select: true,
//   //   select_active: isOptionsMenuOpen,
//   //   select_filled: !!selectValue
//   // })} onPress={toggleOptionsMenu}>
//   //     {selectValue?.name || placeholder}
//   //     {isOptionsMenuOpen && <div style="select__options" >
//   //         {options.map((option) => {
//   //           return <div style="select__oneOption" key={option.key} onClick={handleSelect(option)}>{option.name}</div>
//   //         })}
//   //     </div>}
//   // </RNPickerSelect>)
//   return (
//     <RNPickerSelect 
//          onPress={toggleOptionsMenu}>
//         {selectValue?.name || placeholder}
//         {isOptionsMenuOpen && <div style="select__options" >
//             {options.map((option) => {
//               return <div style="select__oneOption" key={option.key} onClick={handleSelect(option)}>{option.name}</div>
//             })}
//         </div>}
//     </RNPickerSelect>)
// }

export default Dropdown
