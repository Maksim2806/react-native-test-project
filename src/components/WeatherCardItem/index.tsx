import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { getImage } from '../../service/fetchWeatherForecast'
import { formatDateToDesiredValue, formatFahrenheitToCelsius } from '../../utils'

interface Props {
  item: {
    date: Date,
    iconName: string,
    temperature: number,
  };
}

const WeatherCardItem: React.FC<Props> = ({
  item: { date, iconName, temperature }
}) => {
  return (
    <View style={styles.cardContainer}>
      <Text style={styles.cardContainer__date}>
        {formatDateToDesiredValue(date)}
      </Text>
      <View style={styles.cardContainer__imgWrapper}>
        <Image
          style={styles.cardContainer__img}
          source={{uri: getImage(iconName)}}
          accessibilityLabel="weather icon"
        ></Image>
      </View>
      <Text style={styles.cardContainer__weatherTemperature}>
        {formatFahrenheitToCelsius(temperature) > 0
          ? `+${formatFahrenheitToCelsius(temperature)}`
          : formatFahrenheitToCelsius(temperature)}
        &#176;
      </Text>
    </View>
  )
}


const styles = StyleSheet.create({
  cardContainer: {},
  cardContainer__date: {},
  cardContainer__imgWrapper: {},
  cardContainer__img: {},
  cardContainer__weatherTemperature: {}
})

export default WeatherCardItem
